
const url = 'http://lobinhos.herokuapp.com/wolves/'

export function adoptingWolf(adopter_name, adopter_age, adopter_email, id) {
  const body = {
    wolf: {
      adopter_name: adopter_name,
      adopter_age: adopter_age,
      adopter_email: adopter_email
    }
  }

  const config = {
    method: "PATCH",
    body: JSON.stringify(body),
    headers: {
      "Content-Type": "application/json"
    }
  }

  fetch(url + id, config)
    .then(response => response.json())
    .then(result => {
      console.log(result)
      location.href = '/'
    })
    .catch(err => console.error(err))

}