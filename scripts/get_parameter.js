// função pra ler querystring
export function queryString(parameter) {
  var loc = location.search.substring(1, location.search.length);
  var param_value = false;
  var params = loc.split("&");

  for (let i = 0; i < params.length; i++) {
    let param_name = params[i].substring(0, params[i].indexOf('='));
    if (param_name == parameter) {
      param_value = params[i].substring(params[i].indexOf('=') + 1)
    }
  }
  if (param_value) {
    return param_value;
  }
  else {
    return undefined;
  }
}