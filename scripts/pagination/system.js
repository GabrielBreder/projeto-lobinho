import { showWolf } from "../wolf_list/show_wolf.js"
import { listItems } from "../list_items.js"

const paginationGuideNumbers = document.querySelector('.page-numbers')
const wolfSection = document.querySelector('.example-container')
const checkbox = document.querySelector('#check')

const arrowRight = document.querySelector('#arrow-right')
const arrowLeft = document.querySelector('#arrow-left')

export function pagination(pages, wolves) {
  let actualPage = 1

  paginationGuideNumbers.innerHTML = ''
  for (let i = 1; i <= pages; i++) {
    const number = document.createElement('p')

    number.id = i
    number.innerText = i
    paginationGuideNumbers.appendChild(number)
  }

  const allNumbers = paginationGuideNumbers.querySelectorAll('p')
  allNumbers.forEach(p => {
    p.addEventListener('click', () => {
      let param = ''

      if (checkbox.checked) {
        param = '/adopted'
      } else {
        param = ''
      }




      actualPage = p.id

      wolfSection.innerHTML = ''
      const pages = listItems(wolves, p.id, 10)

      pages.forEach(page => {

        showWolf(page, param)
      })
    })
  })

  arrowRight.addEventListener('click', () => {
    let param = ''

    if (checkbox.checked) {
      param = '/adopted'
    } else {
      param = ''
    }


    if (actualPage < pages) {
      actualPage++
      wolfSection.innerHTML = ''
      const pages = listItems(wolves, actualPage, 10)
      pages.forEach(page => {

        showWolf(page, param)
      })
    }

  })

  arrowLeft.addEventListener('click', () => {
    let param = ''

    if (checkbox.checked) {
      param = '/adopted'
    } else {
      param = ''
    }

    if (actualPage > 1) {
      actualPage--
      wolfSection.innerHTML = ''
      const pages = listItems(wolves, actualPage, 10)
      pages.forEach(page => {

        showWolf(page, param)
      })
    }

  })
}

