import { indexWolves } from "./show_wolves.js"

function getWolves() {
  fetch("http://lobinhos.herokuapp.com/wolves")
    .then(response => response.json())
    .then(wolves => {
      const firstRandomWolf = Math.floor(Math.random() * (wolves.length - 1))
      const secondRandomWolf = Math.floor(Math.random() * (wolves.length - 1))

      indexWolves(wolves[firstRandomWolf], wolves[secondRandomWolf])
    })
    .catch(err => console.log(err))
}
getWolves()