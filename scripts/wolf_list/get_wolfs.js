import { showWolf } from "./show_wolf.js"
import { listItems } from "../list_items.js"
import { pagination } from "../pagination/system.js"

const url = 'http://lobinhos.herokuapp.com/wolves/'
const wolfSection = document.querySelector('.example-container')
const checkbox = document.querySelector('#check')

export const getWolves = () => {
  let param = ''

  if (checkbox.checked) {
    wolfSection.innerHTML = ''
    param = '/adopted'
  } else {
    wolfSection.innerHTML = ''
    param = ''
  }


  fetch(url + param)
    .then(response => response.json())
    .then(wolves => {
      const pages = listItems(wolves, 1, 10)
      pages.forEach(page => {
        showWolf(page, param)
      })

      const numberOfPages = Math.ceil(wolves.length / 10)
      pagination(numberOfPages, wolves)
    })
    .catch(err => console.log(err))
}
getWolves('')

