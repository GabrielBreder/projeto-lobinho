import { getWolves } from "./get_wolfs.js"


const paginationGuideNumbers = document.querySelector('.page-numbers')
const wolfSection = document.querySelector('.example-container')
const checkbox = document.querySelector('#check')
checkbox.addEventListener('click', checkboxLoad)


function checkboxLoad() {
  paginationGuideNumbers.innerHTML = ''
  wolfSection.innerHTML = ''

  if (checkbox.checked) {
    getWolves('/adopted')
  } else {
    getWolves('')
  }
}

let i = 0
export function showWolf(wolf, param) {

  const wolfContainer = document.createElement('div')

  wolfContainer.classList.add('wolf-container')
  wolfContainer.innerHTML = `
  <div class="wolf-img-container">
    <div class="wolf-background"></div>
    <div class="wolf-img">
      <img src="${wolf.image_url}" alt="wolf" />
    </div>
  </div>
  <div class="wolf-description"><div class="wolf-header">
      <div class="name-age-container">
        <h3>${wolf.name}</h3>
        <p>${wolf.age} anos</p>
      </div>
    
        <div class="btn btn-white adopt-btn" id=${wolf.id} >
            <p>Adotar</p>
          </div>
       
      </div>
    <div class="wolf-body">
      <p class="wolf-body-p">
        ${wolf.description}
      </p>
    </div>
  `

  if (i % 2 !== 0) {

    wolfContainer.style.flexDirection = 'row-reverse'
    wolfContainer.lastChild.classList.add('row-reverse')
    wolfContainer.lastChild.firstChild.classList.add('row-reverse')
  }
  i++

  wolfSection.appendChild(wolfContainer)



  const btnAdopt = document.querySelectorAll('.adopt-btn')

  if (param === '/adopted') {
    btnAdopt.forEach(btn => {
      btn.classList.add('adopted-btn')
      btn.innerHTML = '<p>Adotado</p>'
    })
  } else {
    btnAdopt.forEach(btn => {
      btn.addEventListener('click', () => {
        window.location = "/pages/wolf_details.html?id=" + btn.id
      })
    })

  }

}



