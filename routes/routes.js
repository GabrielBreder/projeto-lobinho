
// header buttons
const wolfListBtn = document.querySelector('.wolf-list');
wolfListBtn.addEventListener('click', wolfListPage);

const imgHome = document.querySelector('#home');
imgHome.addEventListener('click', indexPage);

const aboutUsBtn = document.querySelector('.about-us');
aboutUsBtn.addEventListener('click', aboutUsPage);

// footer button 
const footerBtn = document.querySelector('#footer-btn');
footerBtn.addEventListener('click', aboutUsPage);

export function indexPage() {
  location.href = "/";
};

export function aboutUsPage() {
  location.href = "/pages/about_us.html";
};

export function addWolfPage() {
  location.href = "/pages/add_wolf.html";

};

export function adoptWolfPage() {
  location.href = "/pages/adopt_wolf.html";
};

export function wolfDetailsPage() {
  location.href = "/pages/wolf_details.html";
};

export function wolfListPage() {
  location.href = "/pages/wolf_list.html";
};

