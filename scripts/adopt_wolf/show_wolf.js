import { queryString } from "../get_parameter.js";
import { getWolfById } from "./get_wolf_by_id.js";
import { adoptingWolf } from "./update_wolf.js";

const id = queryString("id")
getWolfById(id)

const sectionWolf = document.querySelector('section')

export function wolfAttributes(wolf) {
  const wolfDiv = document.createElement('div')
  wolfDiv.innerHTML = `<div class="section-header">
  <img src="${wolf.image_url}" alt="" />
  <div class="title-id">
    <h1>Adote o(a) ${wolf.name}</h1>
    <p>ID:${wolf.id}</p>
  </div>
</div>
<div class="form-body">
  <div class="name-age">
    <div class="name">
      <p>Seu Nome:</p>
      <input type="text" id="adopter-name" />
    </div>
    <div class="age">
      <p>Anos:</p>
      <input type="text" id="adopter-age" />
    </div>
  </div>
  <div class="photo email">
    <p>Email:</p>
    <input type="text" id="adopter-email" />
  </div>
</div>
<div class="form-footer">
  <div class="secondary-btn" id="adopt-btn">
    <p>Adotar</p>
  </div>
</div>`

  sectionWolf.appendChild(wolfDiv)

  const adopterName = document.querySelector('#adopter-name')
  const adopterAge = document.querySelector('#adopter-age')
  const adopterEmail = document.querySelector('#adopter-email')

  const adoptBtn = document.querySelector('#adopt-btn')
  adoptBtn.addEventListener('click', () => {
    const adopter_name = adopterName.value
    const adopter_age = adopterAge.value
    const adopter_email = adopterEmail.value

    adoptingWolf(adopter_name, adopter_age, adopter_email, wolf.id)
  })
}