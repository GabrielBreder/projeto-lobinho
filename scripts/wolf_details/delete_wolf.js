const url = 'http://lobinhos.herokuapp.com/wolves/'

export function deleteWolf(id) {

  fetch(url + id, {
    method: "DELETE"
  })
    .then(response => console.log(response))
    .then(result => {
      console.log(result)
      location.href = '/'
    })
    .catch(err => console.error(err))

}