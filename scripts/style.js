const aboutUsDiv = document.querySelector('.about-us')
const underline = document.querySelector('.about-us-underline')

aboutUsDiv.addEventListener('mouseover', () => {
  underline.style.width = '100%'
})

aboutUsDiv.addEventListener('mouseout', () => {
  underline.style.width = '50%'
})
