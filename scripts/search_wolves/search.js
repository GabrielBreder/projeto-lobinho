import { searchWolvesInApi } from './get_wolves.js'

const searchBtn = document.querySelector('#search-btn')
searchBtn.addEventListener('click', searchWolf)
document.addEventListener("keypress", function (e) {
  if (e.key === 'Enter') {
    searchWolf()
  }
})

const checkbox = document.querySelector('#check')

function searchWolf() {

  const searchInput = document.querySelector('#search-input').value

  if (checkbox.checked) {
    searchWolvesInApi('/adopted', searchInput)
  } else {
    searchWolvesInApi('', searchInput)
  }
}
