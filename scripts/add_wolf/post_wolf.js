
const url = 'http://lobinhos.herokuapp.com/wolves/'

export const postMessages = (name, age, image_url, description) => {
  console.log('aqui')
  const body = {
    wolf: {
      name: name,
      age: age,
      image_url: image_url,
      description: description
    }
  }

  const config = {
    method: "POST",
    body: JSON.stringify(body),
    headers: {
      "Content-Type": "application/json"
    }
  }

  fetch(url, config)
    .then(response => response.json())
    .then(result => {
      console.log(result)
      location.href = '/'
    })
    .catch(err => console.error(err))
}

