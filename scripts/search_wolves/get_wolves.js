import { listItems } from "../list_items.js"
import { showWolf } from "../wolf_list/show_wolf.js"
import { pagination } from "../pagination/system.js"

const url = 'http://lobinhos.herokuapp.com/wolves/'

let wolvesSearch = []


const wolfSection = document.querySelector('.example-container')

export function searchWolvesInApi(param, searchWord) {
  fetch(url + param)
    .then(response => response.json())
    .then(wolves => {
      wolves.forEach(wolf => {
        const str = searchWord
        const searchCapitalized = str[0].toUpperCase() + str.substr(1);

        if (wolf.name.includes(searchWord)) {
          wolvesSearch.push(wolf)
        } else if (wolf.name.includes(searchWord.toUpperCase())) {
          wolvesSearch.push(wolf)
        } else if (wolf.name.includes(searchWord.toLowerCase())) {
          wolvesSearch.push(wolf)
        } else if (wolf.name.includes(searchCapitalized)) {
          wolvesSearch.push(wolf)
        }
      })
    })
    .then(() => {
      wolfSection.innerHTML = ''
      const pages = listItems(wolvesSearch, 1, 10)
      pages.forEach(page => {
        if (page !== undefined) {
          showWolf(page, param)

        }
      })

      const numberOfPages = Math.ceil(wolvesSearch.length / 10)
      pagination(numberOfPages, wolvesSearch)
    })
    .then(wolvesSearch = [])
    .catch(err => console.log(err))
}
