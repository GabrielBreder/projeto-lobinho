import { wolfAttributes } from "./show_wolf_id.js"

const url = 'http://lobinhos.herokuapp.com/wolves/'

export const getWolfById = (id) => {
  fetch(url + id)
    .then(response => response.json())
    .then(wolf => wolfAttributes(wolf))
    .catch(err => console.log(err))

}