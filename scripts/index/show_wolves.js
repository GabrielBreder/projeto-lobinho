const wolfSection = document.querySelector('.example-container')

export function indexWolves(firstWolf, secondWolf) {
  const wolfDiv = document.createElement('div')

  wolfDiv.innerHTML = `<div class="wolf-container">
  <div class="wolf-img-container">
    <div class="wolf-background"></div>
    <div class="wolf-img">
      <img src="${firstWolf.image_url}" alt="wolf" />
    </div>
  </div>
  <div class="wolf-description">
    <div class="wolf-header">
      <h3>${firstWolf.name}</h3>
      <p>Idade: ${firstWolf.age} anos</p>
    </div>
    <div class="wolf-body">
      <p>
        ${firstWolf.description}
      </p>
    </div>
  </div>
</div>
<div class="wolf-container">
  <div class="wolf-img-container">
    <div class="wolf-background"></div>
    <div class="wolf-img">
      <img src="${secondWolf.image_url}" alt="wolf" />
    </div>
  </div>
  <div class="wolf-description">
    <div class="wolf-header">
      <h3>${secondWolf.name}</h3>
      <p>Idade: ${secondWolf.age} anos</p>
    </div>
    <div class="wolf-body">
      <p>
        ${secondWolf.description}
      </p>
    </div>
  </div>
</div>`

  wolfSection.append(wolfDiv)
}