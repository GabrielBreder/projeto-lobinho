import { getWolfById } from "./get_wolf_by_id.js";
import { queryString } from "../get_parameter.js";
import { deleteWolf } from "./delete_wolf.js";

const id = queryString("id");
getWolfById(id)

// modificar o HTML com DOM
const sectionWolf = document.querySelector('.example-container')

export function wolfAttributes(wolf) {
  const wolfDiv = document.createElement('div')
  wolfDiv.innerHTML = `<div class="wolf-header">
  <h3 id="wolf-name">${wolf.name}</h3>
</div>
<div class="wolf-container">
  <div class="img-buttons">
    <div class="wolf-img-container">
      <div class="wolf-background"></div>
      <div class="wolf-img">
        <img src="${wolf.image_url}" alt="wolf" id="wolf-img" />
      </div>
    </div>
    <div class="buttons-container">
      <div class="secondary-btn green" id="btn-adopt" >
        <p id="${wolf.id}">ADOTAR</p>
      </div>
      <div class="secondary-btn red" id="btn-exclude">
        <p id="${wolf.id}">EXCLUIR</p>
      </div>
    </div>
  </div>
  <div class="wolf-description">
    <div class="wolf-body">
      <p id="wolf-description">
        ${wolf.description}
      </p>
    </div>
  </div>
</div>`

  sectionWolf.appendChild(wolfDiv)

  const adoptBtn = document.querySelector('#btn-adopt')
  adoptBtn.addEventListener('click', () => {
    location.href = "/pages/adopt_wolf.html?id=" + wolf.id;
  })

  const excludeBtn = document.querySelector('#btn-exclude')
  excludeBtn.addEventListener('click', () => {
    deleteWolf(wolf.id)
  })
}
