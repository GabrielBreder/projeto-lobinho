import { postMessages } from "../post_wolf.js"

const inputs = document.querySelectorAll('input')
const wolfName = inputs[0]
const wolfAge = inputs[1]
const imgLink = inputs[2]

const descriptionInput = document.querySelector('textarea')

const saveBtn = document.querySelector('#save-btn')

saveBtn.addEventListener('click', () => {
  const name = wolfName.value
  const age = wolfAge.value
  const img = imgLink.value
  const description = descriptionInput.value

  postMessages(name, age, img, description)
})